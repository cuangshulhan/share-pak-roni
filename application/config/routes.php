<?php
defined('BASEPATH') or exit('No direct script access allowed');


$route['default_controller'] = 'main_menu/C_main_menu';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


//------------------------------------------------------------------------- ------------------

//============= LOGIN,LOGOUT,DASHBOARD ===============

$route['login'] 			= 'login/C_login/login';
$route['logout'] 			= 'login/C_login/logout';
$route['dashboard'] 		= 'main_menu/C_main_menu';

$route['MasterReportConfig'] 				= 'MasterReportConfig/C_MasterReportConfig';
// $route['MasterReportConfig/ModalGroup'] 	= 'MasterReportConfig/C_MasterReportConfig/ModalGroup';

<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Master Report Config</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url('dashboard'); ?>">Dashboard</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Master Report Config</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->

    <div class="container-fluid">
        <div class="row">
            <!-- Column -->
            <div class="col-md-12 col-lg-12 col-xlg-3">
                <div class="card">
                    <div class="card-body">
                        <div class="card-body" style="margin-top: -20px;">

                            <div class="row mb-1">
                                <div class="col-md-2">
                                    <label for="recipient-name" class="col-form-label font-weight-bold">Customer:</label>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <select class="select2 form-control custom-select" name="" id="" style="width: 100%;">
                                            <option>Select</option>
                                            <option value="AK">Alaska</option>
                                            <option value="HI">Hawaii</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-2"></div>

                                <div class="col-md-2">
                                    <label for="recipient-name" class="col-form-label font-weight-bold">Category:</label>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <select class="select2 form-control custom-select" name="" id="" style="width: 100%;">
                                            <option>Select</option>
                                            <option value="AK">Alaska</option>
                                            <option value="HI">Hawaii</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-1">
                                <div class="col-md-2">
                                    <label for="recipient-name" class="col-form-label font-weight-bold">Sub Customer:</label>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <select class="select2 form-control custom-select" name="" id="" style="width: 100%;">
                                            <option>Select</option>
                                            <option value="AK">Alaska</option>
                                            <option value="HI">Hawaii</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4"></div>

                                <div class="col-md-3">
                                    <div class="custom-control custom-checkbox mr-sm-2">
                                        <input type="checkbox" class="custom-control-input" id="cekbox1">
                                        <label class="custom-control-label font-weight-bold" for="cekbox1">Set Default</label>

                                        <button type="button" class="btn btn-secondary ml-4" style="width: 90px;" data-toggle="modal" data-target="#exampleModalLong">
                                            <i class="fas fa-chevron-right"></i> <i class="fas fa-chevron-right"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-11">
                                    <div class="table-responsive">
                                        <table id="datatable2" class="datatable2 table table-striped table-bordered ">
                                            <thead>
                                                <tr>
                                                    <th width="10%" class="text-center"><b>Sheet_No</b></th>
                                                    <th width="10%" class="text-center"><b>Sheet_Name</b></th>
                                                    <th width="10%" class="text-center"><b>Table_Name</b></th>
                                                    <th width="10%" class="text-center"><b>Field_Name</b></th>
                                                    <th width="10%" class="text-center"><b>Order_By</b></th>
                                                    <th width="10%" class="text-center"><b>Status</b></th>
                                                    <th width="10%" class="text-center"><b>Parent/Child</b></th>
                                                    <th width="10%" class="text-center"><b>Grouping_Report</b></th>
                                                    <th width="10%" class="text-center"><b>Active</b></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="row mb-1">
                                        <div class="col-md-12">
                                            <button type="button" class="btn btn-success font-weight-bold" style="width: 90px;" data-toggle="modal" data-target="#exampleModalLong">
                                                Add
                                            </button>
                                        </div>
                                    </div>
                                    <div class="row mb-1">
                                        <div class="col-md-12">
                                            <button type="button" class="btn btn-info font-weight-bold" style="width: 90px;" data-toggle="modal" data-target="#exampleModalLong">
                                                Edit
                                            </button>
                                        </div>
                                    </div>
                                    <div class="row mb-1">
                                        <div class="col-md-12">
                                            <button type="button" class="btn btn-primary font-weight-bold" style="width: 90px;" data-toggle="modal" data-target="#exampleModalLong">
                                                AddDetail
                                            </button>
                                        </div>
                                    </div>
                                    <div class="row mb-1">
                                        <div class="col-md-12">
                                            <button type="button" class="btn btn-danger font-weight-bold" style="width: 90px;" data-toggle="modal" data-target="#exampleModalLong">
                                                Delete
                                            </button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <button type="button" class="btn btn-secondary font-weight-bold" style="width: 90px;" data-toggle="modal" data-target="#exampleModalLong">
                                                Group
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card-body" style="margin-top: -15px;">
                            <div class="row mb-1">
                                <div class="col-md-2">
                                    <label for="recipient-name" class="col-form-label font-weight-bold">Sheet No:</label>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="" id="">
                                    </div>
                                </div>

                                <div class="col-md-4"></div>

                                <div class="col-md-3">
                                    <div class="custom-control custom-checkbox mr-sm-2">
                                        <input type="checkbox" class="custom-control-input" id="cekbox2">
                                        <label class="custom-control-label font-weight-bold" for="cekbox2">Create Parent/Child</label>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-1">
                                <div class="col-md-2">
                                    <label for="recipient-name" class="col-form-label font-weight-bold">Sheet Name:</label>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="" id="">
                                    </div>
                                </div>

                                <div class="col-md-2"></div>

                                <div class="col-md-1">
                                    <label for="recipient-name" class="col-form-label font-weight-bold">Grouping:</label>
                                </div>

                                <div class="col-md-1">
                                    <button type="button" class="btn btn-secondary float-left font-weight-bold" style="width: 70px;" data-toggle="modal" data-target="#ModalGroup">
                                        Group
                                    </button>
                                </div>

                                <div class="col-md-3">
                                    <input type="text" class="form-control" id="Group" placeholder="" readonly>
                                </div>
                            </div>

                            <div class="row mb-1">
                                <div class="col-md-2">
                                    <label for="recipient-name" class="col-form-label font-weight-bold">Table Name:</label>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <select class="select2 form-control custom-select" name="" id="" style="width: 100%;">
                                            <option>Select</option>
                                            <option value="AK">Alaska</option>
                                            <option value="HI">Hawaii</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-7"></div>
                            </div>

                            <div class="row mb-1">
                                <div class="col-md-2">
                                    <label for="recipient-name" class="col-form-label font-weight-bold">Field Name:</label>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <select class="select2 form-control custom-select" name="" id="" style="width: 100%;">
                                            <option>Select</option>
                                            <option value="AK">Alaska</option>
                                            <option value="HI">Hawaii</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-2"></div>

                                <div class="col-md-1">
                                    <label for="recipient-name" class="col-form-label font-weight-bold">Order By:</label>
                                </div>
                                <div class="col-md-1">
                                    <button type="button" class="btn btn-secondary float-left font-weight-bold" style="width: 70px;" data-toggle="modal" data-target="#exampleModalLong">
                                        Order
                                    </button></div>

                                <div class="col-md-3">
                                    <input type="text" class="form-control" id="fname" placeholder="">
                                </div>
                            </div>

                            <div class="row mb-1">
                                <div class="col-md-2">
                                    <label for="recipient-name" class="col-form-label font-weight-bold">Status:</label>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <select class="select2 form-control custom-select" name="" id="" style="width: 100%;">
                                            <option>Select</option>
                                            <option value="AK">Alaska</option>
                                            <option value="HI">Hawaii</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-7"></div>
                            </div>

                            <div class="row mb-1">
                                <div class="col-md-2">
                                    <label for="recipient-name" class="col-form-label font-weight-bold">Active:</label>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <select class="select2 form-control custom-select" name="" id="" style="width: 100%;">
                                            <option>Select</option>
                                            <option value="AK">Alaska</option>
                                            <option value="HI">Hawaii</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4"></div>

                                <div class="col-md-3">
                                    <button type="button" class="btn btn-danger float-right ml-2 font-weight-bold" style="width: 90px;" data-toggle="modal" data-target="#exampleModalLong">
                                        Cancel
                                    </button>
                                    <button type="button" class="btn btn-success float-right font-weight-bold" style="width: 90px;" data-toggle="modal" data-target="#exampleModalLong">
                                        Save
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal Grooup -->
    <div class="modal fade bd-example-modal-lg" id="ModalGroup" tabindex="-1" role="dialog" aria-labelledby="ModalGroupTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered " role="document">
            <div class="modal-content" id="app">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Grouping Report</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row mb-1">
                        <div class="col-md-6">
                            <label for="recipient-name" class="col-form-label font-weight-bold">Table Name:</label>
                        </div>
                        <div class="col-md-6">
                            <label for="recipient-name" class="col-form-label font-weight-bold">Field Name:</label>
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col-md-6">
                            <select class="form-control select2" v-model="GroupingTableName">
                                <option disabled>Select</option>
                                <option value="AK">Alaska</option>
                                <option value="HI">Hawaii</option>
                            </select>
                            <!-- <input type="text" class="form-control" v-model:value="GroupingTableName"> -->
                        </div>

                        <div class="col-md-6">
                            <select class="form-control" name="" id="" v-model="GroupingFieldName">
                                <option disabled>Select</option>
                                <option value="AK">Alaska</option>
                                <option value="HI">Hawaii</option>
                            </select>
                        </div>
                    </div>

                    <div class="row mb-1">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table id="ModalGroupData" class="datatable3 table table-striped table-bordered " style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th class="text-center" colspan="2"><b>Group Report</b></th>
                                        </tr>
                                    </thead>
                                    <tbody id="ModalGroupDataBody">
                                        <tr v-for="data in GroupingData">
                                            <td width="80%">{{data.GroupBy}}</td>
                                            <td class="text-center"><button id="GroupDelete" class="btn btn-danger" @click="deleteGroup(data)"><b>Delete</b></button></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" @click="addGroup"><b>Add</b></button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><b>Close</b></button>
                </div>
            </div>
        </div>
    </div>





    <script src="<?php echo base_url('/assets/libs/jquery/dist/jquery.min.js'); ?>"></script>

    <script>
        $(document).ready(function() {
            var penampungGroup = "";
            document.getElementById("Group").value = penampungGroup;
        });

        function GroupData(event) {
            var GroupTemp = "";
            for (let i = 0; i < event.length; i++) {

                if (GroupTemp == "") {
                    GroupTemp = event[i]['GroupBy'];
                } else {
                    GroupTemp = GroupTemp + ', ' + event[i]['GroupBy'];
                }
            }
            penampungGroup = GroupTemp;
            document.getElementById("Group").value = penampungGroup;
            return;
        }

        // =============== Start Vue JS ===============
        var app = new Vue({
            el: "#app",
            data: {
                GroupingTableName: "",
                GroupingFieldName: "",
                GroupingData: []
            },
            methods: {
                addGroup: function() {
                    this.GroupingData.push({
                        'GroupBy': this.GroupingTableName + '.' + this.GroupingFieldName
                    });
                    GroupData(this.GroupingData);
                },
                deleteGroup(event) {
                    let index = this.GroupingData.indexOf(event)
                    this.GroupingData.splice(index, 1);
                    GroupData(this.GroupingData);
                },
            }
        });
        // =============== End Vue Js Script =============== 


        // $('#ModalGroup').click(function() {
        //     {
        //         $.ajax({
        //             url: "<?php echo base_url('MasterReportConfig/ModalGroup'); ?>",
        //             type: 'get',
        //             data: {},
        //             dataType: 'html',
        //             success: function(response) {
        //                 $('#ModalData').html(response);
        //                 $('#ModalData').modal({
        //                     show: true,
        //                     backdrop: 'static'
        //                 });
        //             }
        //         });
        //     }
        // });
    </script>
<div class="modal-dialog modal-dialog-centered " role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Edit Credit Data</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">

            <input type="hidden" name="id_edit" value="{{$id}}">

            <label for="recipient-name" class="col-form-label">Date of entry:</label>
            <div class="input-group">
                <input type="text" class="form-control datepicker-autoclose" id="tanggal_edit" placeholder="mm/dd/yyyy" name="tanggal_edit">
                <div class="input-group-append">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
            </div>
            <div class="form-group">
                <label for="recipient-name" class="col-form-label">Nominal of Credit:</label>
                <input type="text" class="form-control" id="nominal_edit" name="nominal_edit">
            </div>
            <div class="form-group">
                <label for="message-text" class="col-form-label">Description:</label>
                <textarea class="form-control" id="message-text" name="keterangan_edit"></textarea>
            </div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" onclick="closeData()">Close</button>
            <button type="submit" class="btn btn-primary" onclick="updateData()">Update changes</button>
        </div>
    </div>
</div>


<script>
    function closeData() {
        $('#ModalData').modal('hide');
    }
</script>
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_MasterReportConfig extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		//$this->load->model('m_barang');

		// if($this->session->userdata('status_login') != 'masuk')
		// {
		// 	redirect(base_url());
		// }
	}

	public function index()
	{
		$data['konten']  = 'View';

		$this->load->view('main_menu/index', $data);
	}

	public function ModalGroup()
	{
		$this->load->view('ModalGroup');
	}
}

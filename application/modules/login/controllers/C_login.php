<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class C_login extends CI_Controller {

	function __construct(){
        parent::__construct();
        //$this->load->model('m_barang');

    }

	public function index()
	{
		if($this->session->userdata('status_login') != 'masuk')
        {
        	$this->load->view('login');
        }
        else
        {
        	redirect('dashboard');
        }
	}

	public function login()
	{
		$username = $_POST['username'];
		$password = $_POST['password'];

		$cek_login = $this->db->query("SELECT * FROM tpp_m_user WHERE username = '".$username."' AND password = '".$password."' AND Rec_ID = 0 ");

		if($cek_login->num_rows() > 0 )
		{
			$this->session->set_userdata('user_tpp', $username);
			$this->session->set_userdata('status_login', 'masuk');

			redirect('dashboard');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function logout()
	{
		session_unset('user_tpp');
		session_unset('status_login');

		unset($_SESSION['cargo_in']);
		unset($_SESSION['no_cargo_in']);
      	unset($_SESSION['cargo_in_det']);

		unset($_SESSION['cargo_out']);
		unset($_SESSION['no_cargo_out']);
      	unset($_SESSION['cargo_out_det']);

      	unset($_SESSION['id_session_det']);
      	unset($_SESSION['temp_id']);

		redirect(base_url());
	}


}
